package net.allegea.transparentwallpaper;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.os.Build;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

public class CameraView extends SurfaceView implements SurfaceHolder.Callback {

    private SurfaceHolder holder;

    private Camera camera;

    public CameraView(Context context) {

        super(context);

        camera = getCameraInstance();

    }

    public CameraView(Context context, SurfaceHolder holder) {

        this(context);
        this.holder = holder;

        holder.addCallback(this);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
            holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }

    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {

        if(camera == null) {
            camera = getCameraInstance();
        }

        if(camera != null) {
            // The Surface has been created, now tell the camera where to draw the preview.
            try {
                camera.setPreviewDisplay(surfaceHolder);
                camera.startPreview();
            }
            catch (IOException e) {
                Log.e("CameraView", "Error setting camera preview: " + e.getMessage());
            }
        }

    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int width, int height) {

        if (holder.getSurface() == null) {
            Log.i("CameraView", "Holder not created!");
        }
        else {
            if (camera == null) {
                camera = getCameraInstance();
                try {
                    camera.setPreviewDisplay(holder);
                    camera.startPreview();
                }
                catch (Exception e) {
                    Log.e("CameraView", "Error starting camera preview: " + e.getMessage());
                }
            }
        }

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

        if(camera != null) {
            try {
                camera.stopPreview();
                camera.release();
            }
            catch (Exception e) {
                Log.e("CameraView", "Error stopping camera preview: " + e.getMessage());
            }
        }

    }

    private Camera getCameraInstance() {

        Context context = getContext();

        Camera camera = null;

        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            try {
                camera = Camera.open();
            }
            catch (Exception e) {
                Log.e("CameraView", "Error getting camera instance: " + e.getMessage());
            }
        }
        else {
            Log.i("CameraView", "No camera found!");
        }

        return camera;

    }

}