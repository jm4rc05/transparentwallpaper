package net.allegea.transparentwallpaper;

import android.os.Handler;
import android.service.wallpaper.WallpaperService;
import android.view.SurfaceHolder;

import static android.graphics.PixelFormat.OPAQUE;

public class TransparentWallpaperService extends WallpaperService {

    @Override
    public Engine onCreateEngine() {

        return new GlowEngine();

    }

    private class GlowEngine extends Engine {

        private final Handler handler = new Handler();

        private final Runnable viewRunner = new Runnable() {

            @Override
            public void run() {

                drawView();

            }

        };

        private boolean visible = true;

        private CameraView view;

        public GlowEngine() {

            super();

            view = new CameraView(getBaseContext(), getSurfaceHolder());

            handler.post(viewRunner);

        }

        @Override
        public void onVisibilityChanged(boolean visible) {

            this.visible = visible;
            if (visible) {
                handler.post(viewRunner);
            }
            else {
                handler.removeCallbacks(viewRunner);
            }

        }

        @Override
        public void onSurfaceDestroyed(SurfaceHolder holder) {

            super.onSurfaceDestroyed(holder);

            this.visible = false;

            handler.removeCallbacks(viewRunner);

        }

        private void drawView() {

            view.surfaceChanged(getSurfaceHolder(), OPAQUE, view.getWidth(), view.getHeight());

            handler.removeCallbacks(viewRunner);
            if (visible) {
                handler.postDelayed(viewRunner, 5000);
            }

        }

    }

}